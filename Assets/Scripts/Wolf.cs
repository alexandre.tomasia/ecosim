﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wolf : Species
{
    float timeBeforeEating;
    bool canEat;

    const float EATDELAY = 5;


    // Start is called before the first frame update
    void Start()
    {
        timeBeforeEating = EATDELAY;
        canEat = false;
    }

    // Update is called once per frame
    void Update()
    {
        base.Update();

        if (timeBeforeEating > 0)
            timeBeforeEating -= Time.deltaTime;

        else if (timeBeforeEating <= 0 && !canEat)
        {
            canEat = true;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        RandomMove();
    }

    /// <summary>
    /// Collision d'un loup avec une autre GameObject possédant un collider et un rigidBody
    /// </summary>
    /// <param name="other">Collider</param>
    private void OnTriggerEnter(Collider other)
    {
        if (canEat && other.CompareTag("Sheep"))
        {
            Destroy(other.gameObject);
            canEat = false;
            timeBeforeEating = EATDELAY;
            HasEaten();
        }

    }

}
