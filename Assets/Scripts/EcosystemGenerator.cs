﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Classe s'occupant de la génération d'un écosystème initial pour une simulation, à partir de paramètres
/// définis par l'utilisateur.
/// </summary>
public class EcosystemGenerator : MonoBehaviour
{
    #region User Params
    [Header("User parameters")]
    /// <summary>
    /// Nombre de lignes de cubes composant la map.
    /// L'unité utilisé est le nombre de tiles.
    /// </summary>
    public int width;

    /// <summary>
    /// Nombre de colonnes de cubes par ligne composant la map.
    /// L'unité utilisé est le nombre de tiles.
    /// </summary>
    public int height;

    /// <summary>
    /// Pourcentage de tiles d'eau sur la map.
    /// </summary>
    [Range(0f, .8f)]
    public float waterTilesPercentage;

    /// <summary>
    /// Pourcentage de tiles d'obstacles sur la map.
    /// </summary>
    [Range(0f, .5f)] // TODO: changer la limite après le fix
    public float obstacleTilesPercentage;

    /// <summary>
    /// Pourcentage de tiles qui produisent de la nourriture par rapport au nombre de tiles 
    /// n'étant pas des tiles d'eau.
    /// </summary>
    [Range(0f, .5f)] // TODO: changer la limite après le fix
    public float foodProducingTilesPercentage;

    /// <summary>
    /// Nombre initial de proies sur la map.
    /// </summary>
    public int initialPreysAmount;

    /// <summary>
    /// Nombre initial de prédateurs sur la map.
    /// </summary>
    public int initialPredatorsAmount;
    #endregion

    #region Technical Generation Params
    [Header("Generation refs")]
    /// <summary>
    /// Prefab pour les tiles terrestres ne comportant pas d'obstacles (herbe/plaine..).
    /// </summary>
    public GameObject suitableGroundTilePrefab;
    /// <summary>
    /// Prefab pour les tiles terrestres produisant de la nourriture.
    /// </summary>
    public GameObject foodProducingTilePrefab;
    /// <summary>
    /// Prefab pour les tiles représentant une case d'eau.
    /// </summary>
    public GameObject waterTilePrefab;
    /// <summary>
    /// Prefab pour les tiles terrestres comptant un obstacle et n'étant pas accessibles.
    /// </summary>
    public GameObject groundObstacleTilePrefab;
    /// <summary>
    /// Prefab pour une proie.
    /// </summary>
    public GameObject preyPrefab;
    /// <summary>
    /// Prefab pour un prédateur.
    /// </summary>
    public GameObject predatorPrefab;

    /// <summary>
    /// Ref à l'ecosystem manager.
    /// </summary>
    public EcosystemManager ecosystemManager;

    /// <summary>
    /// Valeur utilisée pour parcourir les valeurs du perlinNoise.
    /// Plus cette valeur est petite, moins complexe sera le résultat.
    /// </summary>
    public float increment = 0.05f;

    /// <summary>
    /// Couleur sur laquelle jouer pour faire varier la couleur du sol.
    /// </summary>
    public Color groundTilesColorBase;

    /// <summary>
    /// Seed pour avoir une map différente à chaque génération.
    /// </summary>
    private float seed;

    /// <summary>
    /// Valeurs de perlin noise générées stockées dans un table à 2 dimensions.
    /// </summary>
    private float[,] noiseValues2D;

    /// <summary>
    /// Valeurs de perlin noise générées stockées dans un table à 1 dimension.
    /// </summary>
    private float[] noiseValues1D;

    /// <summary>
    /// Valeur perlin noise à partir de laquelle toutes les valeurs inférieures seront des cases d'eau.
    /// </summary>
    private float waterThreshold;

    /// <summary>
    /// Valeur perlin noise à partir de laquelle toutes les valeurs inférieures seront des cases d'eau.
    /// </summary>
    private float obstaclesThreshold;

    /// <summary>
    /// Valeurs des perlin noises associées à une case productrice de nourriture.
    /// </summary>
    private List<float> foodTiles;

    /// <summary>
    /// Valeurs des perlin noises associées à une case où sera spawn une proie.
    /// </summary>
    private List<float> preysTiles;

    /// <summary>
    /// Valeurs des perlin noises associées à une case où sera spawn un predateur.
    /// </summary>
    private List<float> predatorsTiles;

    // Valeurs associées au nombre de tiles créées par type
    private int waterThresholdIndex;
    private int obstaclesThresholdIndex;
    private int amountOfObstacleTiles;
    private int amountOfFoodTiles;

    /// <summary>
    /// Parent des éléments instanciés de l'écosystème.
    /// </summary>
    private GameObject ecosystemParent;
    #endregion

    #region Main Generation Functions
    /// <summary>
    /// Méthode principale à appeler lorsqu'on veut générer un écosystème.
    /// </summary>
    public void GenerateEcosystem(int myWidth, int myHeight, int nbPrey, int nbPredator, float waterPercentage, float obstaclePercentage, float foodPercentage )
    {
        try
        {
            width = myWidth;
            height = myHeight;
            initialPreysAmount = nbPrey;
            initialPredatorsAmount = nbPredator;
            waterTilesPercentage = waterPercentage;
            obstacleTilesPercentage = obstaclePercentage;
            foodProducingTilesPercentage = foodPercentage;

            // Génération d'une seed pour avoir un perlin noise différent à chaque génération
            seed = UnityEngine.Random.Range(0, 999);

            // Appel des méthodes de génération de la map
            InitGenerationCollections();
            GenerateMap();
            RenderMap();

            this.ecosystemManager.InitEcosystemManager();
        }
        catch (Exception ex)
        {
            // Get stack trace for the exception with source file information
            var st = new System.Diagnostics.StackTrace(ex, true);
            // Get the top stack frame
            var frame = st.GetFrame(0);
            // Get the line number from the stack frame
            var line = frame.GetFileLineNumber();
        }
    }

    /// <summary>
    /// Initialise les collections de la classe qui seront utilisées pour la génération de la map.
    /// </summary>
    private void InitGenerationCollections()
    {
        this.noiseValues1D = new float[width * height];
        this.noiseValues2D = new float[width, height];
        this.foodTiles = new List<float>();
        this.preysTiles = new List<float>();
        this.predatorsTiles = new List<float>();
    }

    /// <summary>
    /// Génère la map à partir des données saisies par l'utilisateur.
    /// </summary>
    private void GenerateMap()
    {
        // Vérification sur les données saisies par l'utilisateur
        if (waterTilesPercentage > 1f || waterTilesPercentage < 0f
            || foodProducingTilesPercentage > 1f || foodProducingTilesPercentage < 0f
            || obstacleTilesPercentage > 1f || obstacleTilesPercentage < 0f
            || (this.initialPreysAmount + this.initialPredatorsAmount) * 2 > width * height) 
        {
            Debug.Log("<color=red>Valeurs saisies par l'utilisateur incorrectes pour la génération.</color>");
        }

        // Génération des valeurs de perlin noise
        GeneratePerlinNoiseValues();

        // WATER
        CalculateWaterTiles();

        // OBSTACLES
        CalculateObstacleTiles();

        // NOURRITURE
        CalculateFoodTiles();

        // PROIES ET PREDATEURS
        // Vérification du cas où il y a plus d'especes à spawn que de places disponibles
        CalculateAnimalsSpawnPosition();

        // Log récapitulatif
        Debug.Log("Generating map : " + width + "x" + height + "=" + width * height + " tiles; " + waterThresholdIndex + " water tiles; " +
            amountOfObstacleTiles + " obstacle tiles; " + amountOfFoodTiles + " food tiles; " + this.preysTiles.Count + " preys; " +
            this.predatorsTiles.Count + " predators");

        // Test final sur la répartition des tiles sur la map 
        int testWaterTilesAmount = Convert.ToInt32(width * height * waterTilesPercentage);
        int testObstacleTilesAmount = Convert.ToInt32(((width * height) - width * height * waterTilesPercentage) * obstacleTilesPercentage);
        int testFoodTilesAmount = Convert.ToInt32(((((width * height) - width * height * waterTilesPercentage)) -
            (((width * height) - width * height * waterTilesPercentage) * obstacleTilesPercentage)) * foodProducingTilesPercentage);
        if (testWaterTilesAmount != waterThresholdIndex || testObstacleTilesAmount != amountOfObstacleTiles || testFoodTilesAmount != amountOfFoodTiles)
        {
            Debug.Log("<color=yellow>Wrong tile repartition during the generation, actually got: " + testWaterTilesAmount + " water tiles; " +
            testObstacleTilesAmount + " obstacle tiles; " + testFoodTilesAmount + " food tiles; </color>");
        }
    }

    /// <summary>
    /// Créer visuellement la map à partir de ce qui a été généré.
    /// </summary>
    private void RenderMap()
    {
        // Instanciation du parent (et suppression s'il existe déja)
        if (this.ecosystemParent != null)
        {
            Destroy(this.ecosystemParent);
            this.ecosystemManager.map.Clear();
        }
        this.ecosystemParent = new GameObject("ecosystemParent");
        this.ecosystemParent.transform.position = new Vector3(0f, 0f, 0f);
        this.ecosystemParent.transform.rotation = Quaternion.identity;
        this.ecosystemParent.transform.parent = this.transform;

        // Variables de test
        int waterTilesTestCount = 0, obstacleTilesTestCount = 0, foodTilesTestCount = 0, emptyTilesTestCount = 0, preysTestCount = 0, predatorsTestCount = 0;

        // Parcours de la map générée et instanciation des tiles
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                GameObject instanciatedTile;

                // WATER
                if (this.noiseValues2D[x, y] < waterThreshold && waterTilesTestCount < this.waterThresholdIndex)
                {
                    waterTilesTestCount++;
                    instanciatedTile = Instantiate(this.waterTilePrefab, new Vector3(x, 0f, y), Quaternion.identity, this.ecosystemParent.transform);
                    instanciatedTile.GetComponent<Tile>().LockTile();
                    // TODO: add specific processes concerning water tiles
                }
                // OBSTACLES
                else if (this.noiseValues2D[x, y] > obstaclesThreshold && obstacleTilesTestCount < this.amountOfObstacleTiles)
                {
                    obstacleTilesTestCount++;
                    instanciatedTile = Instantiate(this.groundObstacleTilePrefab, new Vector3(x, 0f, y), Quaternion.identity, this.ecosystemParent.transform);
                    instanciatedTile.GetComponent<Tile>().LockTile();
                    Renderer rend = instanciatedTile.GetComponent<Renderer>();
                    rend.material.SetColor("_BaseColor", new Color(Mathf.Clamp(this.noiseValues2D[x, y] * this.groundTilesColorBase.r * .8f, 0f, .8f), Mathf.Clamp(1f - this.noiseValues2D[x, y] * this.groundTilesColorBase.g, .3f, .8f), this.groundTilesColorBase.b));
                    // TODO: add specific processes concerning obstacle tiles
                }
                else
                {
                    // FOOD 
                    if (foodTiles.Contains(this.noiseValues2D[x, y]) && foodTilesTestCount < this.amountOfFoodTiles)
                    {
                        foodTilesTestCount++;
                        instanciatedTile = Instantiate(this.foodProducingTilePrefab, new Vector3(x, 0f, y), Quaternion.identity, this.ecosystemParent.transform);
                        Renderer rend = instanciatedTile.GetComponent<Renderer>();
                        rend.material.SetColor("_BaseColor", new Color(Mathf.Clamp(this.noiseValues2D[x, y] * this.groundTilesColorBase.r * .8f, 0f, .8f), Mathf.Clamp(1f - this.noiseValues2D[x, y] * this.groundTilesColorBase.g, .3f, .8f), this.groundTilesColorBase.b));
                        // TODO: add specific processes concerning food tiles
                    }
                    // EMPTY
                    else
                    {
                        emptyTilesTestCount++;
                        instanciatedTile = Instantiate(this.suitableGroundTilePrefab, new Vector3(x, 0f, y), Quaternion.identity, this.ecosystemParent.transform);
                        Renderer rend = instanciatedTile.GetComponent<Renderer>();
                        rend.material.SetColor("_BaseColor", new Color(Mathf.Clamp(this.noiseValues2D[x, y] * this.groundTilesColorBase.r * .8f, 0f, .8f), Mathf.Clamp(1f - this.noiseValues2D[x, y] * this.groundTilesColorBase.g, .3f, .8f), this.groundTilesColorBase.b));
                        // TODO: add specific processes concerning empty tiles
                    }

                    // ANIMALS
                    if (this.preysTiles.Contains(this.noiseValues2D[x, y]) && preysTestCount < this.initialPreysAmount)
                    {
                        preysTestCount++;
                        GameObject instanciatedPrey = Instantiate(this.preyPrefab, new Vector3(x, 1f, y), Quaternion.identity, this.ecosystemParent.transform);
                    }
                    else if (this.predatorsTiles.Contains(this.noiseValues2D[x, y]) && predatorsTestCount < this.initialPredatorsAmount)
                    {
                        predatorsTestCount++;
                        GameObject instanciatedPredator = Instantiate(this.predatorPrefab, new Vector3(x, 1f, y), Quaternion.identity, this.ecosystemParent.transform);
                    }
                }

                // Ajout de la tile à l'ecosystem manager
                this.ecosystemManager.map.Add(instanciatedTile.GetComponent<Tile>());
            }
        }

        // Tests finaux post-instanciation
        if (this.waterThresholdIndex != waterTilesTestCount || this.amountOfObstacleTiles != obstacleTilesTestCount ||
            this.amountOfFoodTiles != foodTilesTestCount || (width * height - (waterTilesTestCount + obstacleTilesTestCount + foodTilesTestCount)) != emptyTilesTestCount ||
            preysTestCount != this.initialPreysAmount || predatorsTestCount != this.initialPredatorsAmount)
        {
            Debug.Log("<color=yellow>Instanciated tiles don't match the generated map values..." +
                "; WATER received " + waterTilesTestCount + " and expected " + this.waterThresholdIndex +
                "; OBSTACLES received " + obstacleTilesTestCount + " and expected " + this.amountOfObstacleTiles +
                "; FOOD TILES received " + foodTilesTestCount + " and expected " + this.amountOfFoodTiles +
                "; EMPTY received " + emptyTilesTestCount + " and expected " + (width * height - (waterTilesTestCount + obstacleTilesTestCount + foodTilesTestCount)) +
                "; PREYS received " + preysTestCount + " and expected " + this.initialPreysAmount +
                "; PREDATORS received " + predatorsTestCount + " and expected " + this.initialPreysAmount + "</color>");
        }
    }
    #endregion

    #region Generation Helper Functions

    /// <summary>
    /// Génération des valeurs de perlin noise à partir de la taille de la map donnée.
    /// Sauvegarde des valeurs dans un tableau 1d trié et un tableau 2d.
    /// </summary>
    private void GeneratePerlinNoiseValues()
    {
        float xoff = 0f;
        int i = 0;

        // Génération d'une valeur perlin noise pour chaque tile de la map 
        // Parcours horizontal
        for (int x = 0; x < width; x++)
        {
            xoff += increment;
            float yoff = 0f;   // Pour chaque xoff, on reset yoff à 0

            // Parcours vertical
            for (int y = 0; y < height; y++)
            {
                yoff += increment;
                float noiseVal = Mathf.PerlinNoise(seed + xoff, seed + yoff);

                this.noiseValues2D[x, y] = noiseVal;
                this.noiseValues1D[i] = noiseVal;
                i++;
            }
        }

        // Test sur la longueur du parcours effectué
        if (i != width * height)
            Debug.LogError("Parcours pour génération des perlin noises incorrect.");

        // Determine thresholds : on trie le tableau à une dimension puis on sélectionne la valeur 
        // à l'index associé au pourcentage de tile de type eau par exemple
        Array.Sort(this.noiseValues1D);
    }

    /// <summary>
    /// Détermine la position des tiles d'eau.
    /// </summary>
    private void CalculateWaterTiles()
    {
        waterThresholdIndex = Convert.ToInt32(this.noiseValues1D.Length * waterTilesPercentage);
        waterThreshold = this.noiseValues1D[waterThresholdIndex];
    }

    /// <summary>
    /// Détermine la position des tiles obstacles.
    /// </summary>
    private void CalculateObstacleTiles()
    {
        int amountOfRemainingTiles = this.noiseValues1D.Length - waterThresholdIndex;
        amountOfObstacleTiles = Convert.ToInt32(amountOfRemainingTiles * obstacleTilesPercentage);
        obstaclesThresholdIndex = this.noiseValues1D.Length - amountOfObstacleTiles - 1;
        obstaclesThreshold = this.noiseValues1D[obstaclesThresholdIndex];
    }

    /// <summary>
    /// Détermine la position des tiles produisant de la nourriture.
    /// </summary>
    private void CalculateFoodTiles()
    {
        amountOfFoodTiles = Convert.ToInt32(((((width * height) - width * height * waterTilesPercentage)) -
                    (((width * height) - width * height * waterTilesPercentage) * obstacleTilesPercentage)) * foodProducingTilesPercentage);
        List<int> randomFoodTilesIndexes = GenerateUniqueRandoms(amountOfFoodTiles, this.waterThresholdIndex, this.obstaclesThresholdIndex);
        for (int r = 0; r < randomFoodTilesIndexes.Count; r++)
        {
            this.foodTiles.Add(this.noiseValues1D[randomFoodTilesIndexes[r]]);
        }
    }

    /// <summary>
    /// Détermine la position des proies et prédateurs.
    /// </summary>
    private void CalculateAnimalsSpawnPosition()
    {
        int preysToSpawn = Mathf.Clamp(this.initialPreysAmount, 0, this.obstaclesThresholdIndex - this.waterThresholdIndex);
        int predatorsToSpawn = Mathf.Clamp(this.initialPredatorsAmount, 0, this.obstaclesThresholdIndex - this.waterThresholdIndex - preysToSpawn);
        List<int> randomAnimalsIndexes = GenerateUniqueRandoms(preysToSpawn + predatorsToSpawn, this.waterThresholdIndex, this.obstaclesThresholdIndex);
        for (int r = 0; r < randomAnimalsIndexes.Count; r++)
        {
            if (preysToSpawn > 0)
            {
                this.preysTiles.Add(this.noiseValues1D[randomAnimalsIndexes[r]]);
                preysToSpawn--;
            }
            else if (predatorsToSpawn > 0)
            {
                this.predatorsTiles.Add(this.noiseValues1D[randomAnimalsIndexes[r]]);
                predatorsToSpawn--;
            }
        }
    }

    /// <summary>
    /// Génère une liste de "count" entiers aléatoires UNIQUES, compris entre "min" et "max".
    /// </summary>
    /// <param name="count">Nombre d'entiers aléatoires uniques à générer.</param>
    /// <param name="min">Borne min.</param>
    /// <param name="max">Borne max (EXCLUE).</param>
    /// <returns>Liste d'entiers générés.</returns>
    public List<int> GenerateUniqueRandoms(int count, int min, int max)
    {
        System.Random random = new System.Random();

        if (max <= min || count < 0 ||
                // max - min > 0 required to avoid overflow
                (count > max - min && max - min > 0))
        {
            // need to use 64-bit to support big ranges (negative min, positive max)
            throw new ArgumentOutOfRangeException("Range " + min + " to " + max +
                    " (" + ((long)max - (long)min) + " values), or count " + count + " is illegal");
        }

        // generate count random values.
        HashSet<int> candidates = new HashSet<int>();

        // start count values before max, and end at max
        for (int top = max - count; top < max; top++)
        {
            // May strike a duplicate.
            // Need to add +1 to make inclusive generator
            // +1 is safe even for MaxVal max value because top < max
            if (!candidates.Add(random.Next(min, top + 1)))
            {
                // collision, add inclusive max.
                // which could not possibly have been added before.
                candidates.Add(top);
            }
        }

        // load them in to a list, to sort
        List<int> result = candidates.ToList();

        // shuffle the results because HashSet has messed
        // with the order, and the algorithm does not produce
        // random-ordered results (e.g. max-1 will never be the first value)
        for (int i = result.Count - 1; i > 0; i--)
        {
            int k = random.Next(i + 1);
            int tmp = result[k];
            result[k] = result[i];
            result[i] = tmp;
        }
        return result;
    }
    #endregion
}
