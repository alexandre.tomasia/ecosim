﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodTile : Tile
{
    /// <summary>
    /// Prefab pour de la nourriture à spawn sur la tile.s
    /// </summary>
    public GameObject foodPrefab;

    ///<summary>
    /// Frequence de production de nouriture de la tile
    ///</summary>
    private float productionRate;

    ///<summary>
    /// Ref correspondant à la nourriture instanciée.
    ///</summary>
    private GameObject spawnedFood;

    // Start is called before the first frame update
    void Start()
    {
        productionRate = UnityEngine.Random.Range(5f, 10f);
        spawnedFood = null;
        StartCoroutine(HandleFoodSpawning());
    }

    /// <summary>
    /// Coroutine qui gère le spawn de nourriture.
    /// </summary>
    /// <returns></returns>
    private IEnumerator HandleFoodSpawning()
    {
        while (true)
        {
            yield return new WaitForSeconds(productionRate);
            if (this.spawnedFood == null)
            {
                this.spawnedFood = Instantiate(this.foodPrefab, new Vector3(this.gameObject.transform.position.x, .7f, this.gameObject.transform.position.z), Quaternion.identity, this.transform);
            }
        }
    }

    /// <summary>
    /// Fonction à appeler depuis un mouton.
    /// Enlève la nourriture de la case.
    /// </summary>
    public void RemoveFoodFromTile()
    {
        Destroy(this.spawnedFood);
    }

    /// <summary>
    /// Retourne si de la nourriture est dispo ou pas.
    /// </summary>
    /// <returns></returns>
    public bool IsFoodAvailable()
    {
        return this.spawnedFood != null;
    }
}
