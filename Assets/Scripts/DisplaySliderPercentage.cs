﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplaySliderPercentage : MonoBehaviour
{
    Text myStringValue;

     void Start()
    {
        myStringValue = GetComponent<Text>();
    }

    public void UpdatePercentageValue(float value)
    {
        myStringValue.text = Mathf.Round(value * 100) + " %";
    }
 

}
