﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EcosystemUiManager : MonoBehaviour
{
    /// <summary>
    /// Width/Height maximale.
    /// </summary>
    public int maxDimension = 150;

    // Refs aux input fields
    public InputField widthInputField;
    public InputField heightInputField;
    public InputField preyInputField;
    public InputField predatorInputField;

    // Sliders 
    public Slider waterPercentageSlider;
    public Slider obstaclePercentageSlider;
    public Slider foodPercentageSlider;

    // Colors
    Color valid;
    Color invalid;

    public EcosystemGenerator ecosystemGenerator;

    private void Start()
    {
        invalid = new Color(255.1f, 0.4f, 0.3f, 230f);
        valid = new Color(255f, 255f, 255f, 255f);
    }

    /// <summary>
    /// Check si la dimension est correcte par rapport aux contraintes.
    /// </summary>
    /// <param name="inputField"></param>
    public void CheckDimension(InputField inputField)
    {
        if (inputField.text == "")
            return;

        if (int.TryParse(inputField.text, out int dim))
        {
            if (dim > maxDimension || dim <= 0)
            {
                inputField.image.color = invalid;

            }
            else
            {
                inputField.image.color = valid;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public void GenerateCompleteEcosystem()
    {
        if (ValidParametersForGeneration())
        {
            ecosystemGenerator.GenerateEcosystem(int.Parse(widthInputField.text), int.Parse(heightInputField.text), int.Parse(preyInputField.text),
                int.Parse(predatorInputField.text), this.waterPercentageSlider.value, this.obstaclePercentageSlider.value, this.foodPercentageSlider.value);
            SwitchPanelVisibility();
        }
    }

    /// <summary>
    /// Check avant la génération d'une map.
    /// </summary>
    private bool ValidParametersForGeneration()
    {
        float nbIndivPossible = (int.Parse(widthInputField.text) * int.Parse(heightInputField.text) * (1 - this.waterPercentageSlider.value)) * (1 - this.obstaclePercentageSlider.value);
        int indivTotal = int.Parse(preyInputField.text) + int.Parse(predatorInputField.text);

        if (indivTotal > Mathf.RoundToInt(nbIndivPossible))
        {
            preyInputField.image.color = invalid;
            predatorInputField.image.color = invalid;
            return false;
        }
        else
        {
            preyInputField.image.color = valid;
            predatorInputField.image.color = valid;
            return true;
        }
    }

    /// <summary>
    /// Affiche ou cache le panel courant.
    /// </summary>
    public void SwitchPanelVisibility()
    {
        this.gameObject.SetActive(!this.gameObject.activeInHierarchy);
    }
}
