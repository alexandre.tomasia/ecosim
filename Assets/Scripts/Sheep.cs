using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sheep : Species
{

    /// <summary>
    /// Enumération contenant les différents etats possible pour l'IA 
    /// </summary>
    enum state { MOVING, ESCAPING, REACHING_FOOD, REACHING_WATER};

    // Update is called once per frame
    void FixedUpdate()
    {
        RandomMove();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Food"))
        {
            Destroy(other.gameObject);
            HasEaten();
        }
    }
}
