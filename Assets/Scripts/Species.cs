using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Species : MonoBehaviour
{
    EcosystemManager ecosystemManager;

    enum STATE
    {
        random,
        hungry,
    };

    #region ForLater
    /// <summary>
    /// Portée a laquelle une espéce peut repérer un élément
    /// </summary>
    float visualRange;

    /// <summary>
    /// Niveau de faim de l'espèce 
    /// </summary>
    float hunger;

    /// <summary>
    /// niveau de soif de l'espèce
    /// </summary>
    float thirst;

    /// <summary>
    /// Age de l'espèce
    /// </summary>
    int age;
    #endregion

    #region Movement
    /// <summary>
    /// temps qu'il faudra attendre entre deux déplacement
    /// </summary>
    float moveCooldown = 0.5f;
    /// <summary>
    /// vitesse de l'animation de déplacement
    /// </summary>
    int movementSpeed = 3;
    /// <summary>
    /// le temps du prochain déplacement
    /// </summary>
    float nextMovement;
    /// <summary>
    /// Indique si l'espéce peut se déplacer ou non
    /// </summary>
    public bool canMove;
    /// <summary>
    /// Indique si l'espéce est en train de se déplacer ou non
    /// </summary>
    bool moving;

    bool directionChoosed;

    /// <summary>
    /// Coordonnées de la case dans laquelle l'espece souhaite se déplacer
    /// </summary>
    Vector3 target;

    #endregion

    #region Eat
    /// <summary>
    /// Timer qui décompte le temps restant à vivre au specimen s'il ne mange pas
    /// </summary>
    float timerMeal;
    /// <summary>
    /// Temps total pour chercher à manger avant de mourir
    /// </summary>
    const float TIMETOSEARCHMEAL = 20;
    #endregion

    private void Awake()
    {
        // On récuperer ici l'ecosystemManager qui va servir aussi pour les classes filles
        ecosystemManager = GameObject.Find("EcosystemManager").GetComponent<EcosystemManager>();
        moving = false;
        canMove = true;
        directionChoosed = false;
        moveCooldown = Random.Range(0.5f, 1f);
        timerMeal = TIMETOSEARCHMEAL;
    }




    public void Update()
    {
        TickMealTime();
    }


    /// <summary>
    /// Permet a une espèce de se déplacer de mannière aléatoire sur la map. 
    /// </summary>
    public void RandomMove()
    {
        // ON CHOISI UNE DIRECTION
        if (!directionChoosed)
        {
            choseTarget();
        }
        
        // ON REGARDE SI ON PEUT SE DEPLACER
        // Si on peut bouger et que le cooldown est terminer alors on se déplace
        if (canMove && nextMovement <= Time.time)
        {
            // Si le déplacement sur la Tile choisie est possible on se déplace
            if (this.AbleToMove(target))
            {
                // On reserve la tile sur laquelle on va se déplacer
                ecosystemManager.tilesCoordinate[(int)target.x, (int)target.z].LockTile();

                // On mets moving a true puisqu'on va se déplacer et on dit que l'on ne peut plus bouger
                // Pour ne pas relancer la fonction pendant un déplacement qui est deja en cours
                moving = true;
                canMove = false;

                // On débloque la case que l'on s'apprète a quitter
                ecosystemManager.tilesCoordinate[(int)transform.position.x, (int)transform.position.z].UnlockTile(); 
            }
            // Si le déplacement n'est pas possible on choisi a nouveau une direction
            else
            {
                directionChoosed = false;
                canMove = false;
            }
        }

        // ON SE DEPLACE 
        if (moving)
        {
            SmoothMovement();
        }
    }

    public void SmoothMovement()
    {
        float sqrRemainingDistance = (target - this.transform.position).sqrMagnitude;

        // Tant que l'on est pas sur la bonne tile on continue de se déplacer
        if (sqrRemainingDistance > float.Epsilon)
        {
            this.transform.position = Vector3.MoveTowards(this.transform.position, target, Time.deltaTime * movementSpeed);
            sqrRemainingDistance = (target - this.transform.position).sqrMagnitude;
        }
        else
        {
            moving = false;
            directionChoosed = false;
            canMove = false;
            nextMovement = Time.time + moveCooldown;
        }
    }

    public void choseTarget()
    {
        // La position de l'espèce
        Vector3 position = this.transform.position;

        // On choisi une direction dans vers laquelle se déplacer et on la stocke dans un Vector3
        int xDirection = (int)Random.Range(-1, 2);
        int zDirection = (int)Random.Range(-1, 2);
        Vector3 direction = new Vector3(xDirection, 0, zDirection);

        // La position d'arrivée
        this.target = position + direction;
        directionChoosed = true;
        canMove = true;
    }


    /// <summary>
    /// Fonction qui verifie si il est possible de bouger sur une tile donnée
    /// </summary>
    /// <param name="target">Vector3 représentant la position a laquelle on souhaite se déplacer</param>
    /// <returns>TRUE si le mouvement est possible FALSE sinon</returns>
    public bool AbleToMove(Vector3 target)
    {
        // On verifie que l'on n'essaye pas de se déplacer hors des limites Max de la Map
        if (target.x >= ecosystemManager.tilesCoordinate.GetLength(0) || target.z >= ecosystemManager.tilesCoordinate.GetLength(1))
            return false;

        // On vérifie que l'on n'essaye pas de se déplacer hors des limites Min de la Map
        if (target.x < 0 || target.z < 0)
            return false;

        // On verifie que la Tile sur laquelle on veut se déplacer n'est pas déja reservée.
        if (!ecosystemManager.tilesCoordinate[(int)target.x, (int)target.z].IsReachable())
            return false;
        
        return true;
    }


    /// <summary>
    /// Durée de vie du specimen, s'il n'a pas mangé au bout de X secondes, il meurt
    /// </summary>
    public void LifeLeft()
    {
        if(timerMeal <= 0)
        {
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// Timer pour que le temps s'écoule après un repas d'un specimen
    /// </summary>
    public void TickMealTime()
    {
        timerMeal -= Time.deltaTime;
        LifeLeft();
    }

    /// <summary>
    /// Appel lorsqu'un specimen a mangé pour réinitialiser son timeSinceMeal
    /// </summary>
    public void HasEaten()
    {
        timerMeal = TIMETOSEARCHMEAL;
    }

}
