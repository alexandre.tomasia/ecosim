using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EcosystemManager : MonoBehaviour
{
    /// <summary>
    /// Référence au générateur d'écosystème
    /// </summary>
    public EcosystemGenerator ecosystemGenerator;

    ///<summary>
    /// Liste de Tiles regroupant chaque tile de la map
    ///</summary>
    [HideInInspector]
    public List<Tile> map;

    /// <summary>
    /// Tableau de Tiles dont les index représente la coordonée de la Tile dans la map
    /// </summary>
    public Tile[,] tilesCoordinate;

    /// <summary>
    /// TimeScale pour la simulation.
    /// </summary>
    [Range(1, 15)]
    public float timeScale = 1;

    public void InitEcosystemManager()
    {
        // On initialise notre tableau des coordonnées. Il faut que les dimentions du tableau corresponde aux dimentions de la MAP
        tilesCoordinate = new Tile[ecosystemGenerator.width, ecosystemGenerator.height];

        // On mets chaque tile de la map a sa bonne coordonnée dans le tableau des coordonnées
        foreach (Tile tile in map){
            tilesCoordinate[(int)tile.transform.position.x, (int)tile.transform.position.z] = tile;
        }
    }
}