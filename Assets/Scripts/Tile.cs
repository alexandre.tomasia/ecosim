﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    ///<summary>
    /// Booléen permetant de savoir si une case est accessible ou non.
    ///</summary>
    private bool reachable = true;

    ///<summary>
    /// Renvoie true si un case est accéssible, false sinon.
    ///</summary>
    ///<returns></returns>
    public bool IsReachable()
    {
        return this.reachable;
    }

    /// <summary>
    /// Permet de reserver la Tile empechant le déplacement d'une autre même espece sur cette position
    /// </summary>
    public void LockTile()
    {
        this.reachable = false;
    }

    /// <summary>
    /// Permet de débloqué la Tile permetant le déplacement d'une même autre espece sur cette position
    /// </summary>
    public void UnlockTile()
    {
        this.reachable = true;
    }
}
